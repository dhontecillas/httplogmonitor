# HTTP log monitoring console program

Simple console program that monitors HTTP traffic by reading log
file on the machine.

## Building the app

```
make build
```

## Running the app as a docker container

The `Dockerfile` is two-step:
- first will build the `httplogmonitor` executable from `golang:alpine`
- will create a from `scratch` container for the tool

```
make docker
```

Then you can run:
```
make dockerrun
```

Or, what is the same
```
mkdir tmp
touch tmp/access.log
docker run -v $(pwd)/tmp:/tmp httpmonitor
```

and then you can start updating your local `./tmp/access.log` file to see 
the updates in the running `httplogmonitor`.


### Command line options:

- `-file [filename]`: File to read from (defaults to `/tmp/access.log`)
- `-refresh [seconds]`: Display refresh time in seconds (defaults to 10s).
- `-statstimewindow [seconds]`: Stats time window in seconds (defaults to 10s).
- `-alerttimewindow [seconds]`: Amount of time where if on average is above rate limit, and alert should be triggered.
- `-alertreqpersec [seconds]`: What is the rate limit for the alarm.


## Assumptions:

### Lines ARE ALWAYS in increasing time

The log lines in the log file **ARE ALWAYS in increasing time**: an
older entry can not be found (A log entry with a later time can not appear
after a log entry with an earlier time).


### Time is DISCRETE (but Earth is not flat :)

The resolution that the log entries give us is of **1 second**, so it can
be though as each entry falls into a _1-second bucket_.

As an **example**, lets look at stats for a **2 second time window**:

![time is discrete](docs/time_discrete.png)

Each log entry happens at a moment in time marked by the green arrows, so
if we ask for stats at _**t=2.6**_ (purple arrow), we can not take the stats
from that point to two seconds before (red box marking the time window),
because we don't know which event from bucket _**t=0**_ are included.

So, we have to truncate the time to _**t=2.0**_ and compute stats for the
buckets _**t=1**_ and _**t=0**_ (purple box marking the time window).


### Max request rate is allowed

To trigger an Alert, the rate must be higher (but not equal) to the given
max requests per second.


### A Page

Since this prints to a console, the start of a page will be marked with
a new line starting with `===== PAGE` and end of a page will be marked
with `---- End Of Page`

Example:

```
==== PAGE [4] ==== [2019-03-05 20:25:22.418521952 +0100 CET m=+50.001415029]
GLOBAL - Num Reqs: 8375 Num Bytes: 1030036   STATUS: OK: 8374   Server Err: 1  Others: 0
At: 05/Mar/2019:20:25:22 +0100 (-10s) Hits: 40 Bytes: 4920
TOP 1:
- report : 40

ALERTS:

High traffic generated an alert - hits = 24, triggered at 05/Mar/2019:16:00:56 +0100  ✔ (05/Mar/2019:20:24:33 +0100)
High traffic generated an alert - hits = 24, triggered at 05/Mar/2019:20:25:11 +0100  ✗
---- End of page [4] ---- [2019-03-05 20:25:22.418521952 +0100 CET m=+50.001415029]
```

----

## Application Design

The application is built around the concept of a processing pipeline.

There is an executable (`cmd/httplogmonitor/main.go`) that reads params
from the command line and updates the values for the configuration
(`pkg/config`). Once loaded, it runs the pipeline, by simply calling
`RunPipeline` with the configuration.

The `pkg/pipeline` package is where, based on the `Config` the pipeline
is created using Go channels.

In this package there are wrappers to
consume from channels, and send outputs to other channels. This way,
other packagers are not coupled to this architecture of using channels
(except for the file lines reader, that uses a channel because it listens
to write updates to a file).

Then there are different packages:

- `httplog`: domain entity definition
- `ingest`: package to read from files, and parsing lines into `httplog.Entry`s
- `stats`: package to produce stats based on `http.Entry`s
- `alerts`: package to trigger alerts based on `http.Entry`s
- `output`: package to display or consume stats and alerts

### The application.

Creates a context with cancel, and then "subscribes" to termination
signals, to cancel the all the pipeline, so
_**A user can keep the app running and monitor the log file continuously**_.

### The Pipeline

![processing pipeline](docs/pipeline.png)


### Ingest

#### File Ingest

Given the polling ratio, there were two options, to keep polling the file,
or to use notifications from the filesystem to know when a file has been
updated.

In this case, it uses the [fsnotify](https://github.com/fsnotify/fsnotify)
package to detect when the file has been modified.

- Stores the last read line offset, so when it gets a notification
  of a modified file, it can continue from the last line.

- It is assumed that the file will be updated with full lines
  (at some point it does not have a half written line, because
  in that case we could not use the buffered io, and we
  would need to parse the file ourselves)

#### Parsing

- `ParserW3C.go` consumes an actively [written-to w3c-formatted HTTP access log](https://www.w3.org/Daemon/User/Config/Logging.html).
  It should defaults to reading `/tmp/access.log` and be overrideable

## Stats

There is a `Collector` that updates the "sliding window" stats, and also global stats.

A **section** is defined as being what's before the second '/' in the resource section of the log line.
For example, the section for `/pages/create` is `/pages`.


## Alerts

Each alert is displayed in its own line:

An alert that has passed is displayed with a tick, and the date when the alert stopped:

```
High traffic generated an alert - hits = 24, triggered at 05/Mar/2019:16:00:56 +0100  ✔ (05/Mar/2019:20:24:33 +0100)
```

If the alert is active, a cross is shown at the end of the line, and no time for stopped:

```
High traffic generated an alert - hits = 24, triggered at 05/Mar/2019:20:25:11 +0100  ✗
```

## Improvements

- Better handling of errors (with typed errors)
- Improve test coverage.
- Allow arrival of non sequential Entries
- Allow inputs from several files.
- Add a better console display with colors.
- Add better global stats:
    - Number of requests for each method
    - Average size of responses per section
- More diverse alerting:
    - Have different alert thresholds for different section / paths
    - Alerts depending on the status codes
-----

FROM golang:alpine AS build-env
COPY  . /go/src/gitlab.com/dhontecillas/httplogmonitor
WORKDIR /go/src/gitlab.com/dhontecillas/httplogmonitor
RUN apk add make
RUN apk add git
RUN make tools
RUN touch access.log
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o httplogmonitor ./cmd/httplogmonitor

FROM scratch
COPY --from=build-env /go/src/gitlab.com/dhontecillas/httplogmonitor/access.log /var/log/access.log
COPY --from=build-env /go/src/gitlab.com/dhontecillas/httplogmonitor/access.log /tmp/access.log
COPY --from=build-env /go/src/gitlab.com/dhontecillas/httplogmonitor/httplogmonitor /app/httplogmonitor
WORKDIR /app
ENTRYPOINT ["/app/httplogmonitor"]

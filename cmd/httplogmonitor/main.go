package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/config"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/pipeline"
)

func main() {
	fmt.Println("# ==========[httplogmonitor]========== #")
	c := config.NewConfig()
	overrideConfigFromArguments(&c)

	// Create a context with cancel, and listen for a termination signal
	ctx, cancel := context.WithCancel(context.Background())
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		<-sigc
		cancel()
	}()

	pipeline.RunPipeline(ctx, c)

	<-ctx.Done()
}

func overrideConfigFromArguments(c *config.Config) {
	flag.StringVar(&c.FileName, "file", c.FileName, "File to read from")
	flag.IntVar(&c.DisplayRefreshTime, "refresh", c.DisplayRefreshTime, "Display refresh time in seconds (defaults to 10s)")
	flag.IntVar(&c.StatsTimeWindow, "statstimewindow", c.StatsTimeWindow, "Stats time window in seconds (defaults to 10s)")
	flag.IntVar(&c.AlertConf.TimeWindow, "alerttimewindow", c.AlertConf.TimeWindow, "Alert rate limit time window (defaults to 120s)")
	flag.IntVar(&c.AlertConf.MaxRequestsPerSec, "alertreqpersec", c.AlertConf.MaxRequestsPerSec, "Rate limit for the alert")
	flag.Parse()
}

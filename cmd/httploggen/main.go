package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type config struct {
	FileName         string
	MinReqsPerSecond int
	MaxReqsPerSecond int
}

type entry struct {
	RemoteHost string
	RemoteUser string
	User       string
	Date       time.Time
	Method     string
	Path       string
	Status     int
	Bytes      int
}

func newEntry() *entry {
	return &entry{
		RemoteHost: "127.0.0.1",
		RemoteUser: "-",
		User:       "jame",
		Date:       time.Now(),
		Method:     "GET",
		Path:       "/report",
		Status:     200,
		Bytes:      123,
	}
}

func (e *entry) String() string {
	return fmt.Sprintf("%s %s %s [%s] \"%s %s HTTP/1.0\" %d %d\n", e.RemoteHost, e.RemoteUser, e.User,
		e.Date.Format("02/Jan/2006:15:04:05 -0700"), e.Method, e.Path, e.Status, e.Bytes)
}

func main() {
	c := config{FileName: "./tmp_file.log"}
	overrideconfigFromArguments(&c)

	// Create a context with cancel, and listen for a termination signal
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	ticker := time.NewTicker(time.Duration(1) * time.Second)

	e := newEntry()
	run := true
	for run {
		select {
		case <-ticker.C:
			for i := 0; i < c.MinReqsPerSecond; i++ {
				f, err := os.OpenFile(c.FileName, os.O_RDWR|os.O_CREATE, 0644)
				if err != nil {
					panic("Can not open file")
				}
				e.Date = time.Now()
				_, _ = f.Seek(0, 2)
				_, _ = f.WriteString(e.String())
				_ = f.Sync()
				_ = f.Close()
			}
		case <-sigc:
			run = false
		}
	}
}

func overrideconfigFromArguments(c *config) {
	flag.StringVar(&c.FileName, "file", c.FileName, "File to write to")
	flag.IntVar(&c.MinReqsPerSecond, "min", 1, "Min requests per second")
	flag.IntVar(&c.MaxReqsPerSecond, "max", 1, "Max requests per second")
	flag.Parse()
}

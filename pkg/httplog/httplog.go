package httplog

import "time"

// Entry contains information about an http request found in the log
type Entry struct {
	RemoteHost string
	RemoteUser string
	AuthUser   string
	Date       time.Time
	Status     int
	Bytes      int
	Method     string
	Path       []string
}

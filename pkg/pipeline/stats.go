package pipeline

import (
	"context"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/stats"
)

// StatsUpdater gets log entries and outputs stats updates
func StatsUpdater(ctx context.Context, in chan httplog.Entry, out chan stats.Stats, c stats.Collector) {
	// update the stats each second, no matter the display refresh rate
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	for {
		select {
		case e := <-in:
			c.Add(e)
		case <-ticker.C:
			out <- c.Update(time.Now())
		case <-ctx.Done():
			return
		}
	}
}

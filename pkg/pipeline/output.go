package pipeline

import (
	"context"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/alerts"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/output"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/stats"
	"time"
)

// DisplayOutput receives stats and alerts and  displays them every refreshSeconds
func DisplayOutput(ctx context.Context, inStats chan stats.Stats, inAlerts chan alerts.Alert,
	outStats output.StatsOutputer, outAlerts output.AlertsOutputer, refreshSeconds int) {
	ticker := time.NewTicker(time.Duration(refreshSeconds) * time.Second)
	defer ticker.Stop()
	for {
		select {
		case s := <-inStats:
			outStats.SetStats(s)
		case a := <-inAlerts:
			outAlerts.AlertNotification(a)
		case <-ticker.C:
			outStats.Display()
			outAlerts.Display()
		case <-ctx.Done():
			return
		}
	}
}

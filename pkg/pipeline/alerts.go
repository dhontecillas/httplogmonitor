package pipeline

import (
	"context"
	"log"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/alerts"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
)

// AlertsUpdater gets log entries and outputs stats updates
func AlertsUpdater(ctx context.Context, in chan httplog.Entry, out chan alerts.Alert, alerter alerts.Alerter) {
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	for {
		select {
		case e := <-in:
			newAlerts, err := alerter.Add(e)
			if err == nil {
				for _, a := range newAlerts {
					out <- a
				}
			} else {
				log.Printf("can not ingest Entry for alerts: %s", err)
			}
		case <-ticker.C:
			newAlerts := alerter.Update(time.Now())
			for _, a := range newAlerts {
				out <- a
			}
		case <-ctx.Done():
			return
		}
	}
}

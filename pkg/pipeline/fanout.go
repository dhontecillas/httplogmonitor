package pipeline

import (
	"context"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
)

// HTTPLogEntryFanOut gets an httplog.Entry from the input channel,
// and sends it to each output channel.
func HTTPLogEntryFanOut(ctx context.Context, in chan httplog.Entry, out ...chan httplog.Entry) {
	for {
		select {
		case e := <-in:
			for idx := range out {
				out[idx] <- e
			}
		case <-ctx.Done():
			return
		}
	}
}

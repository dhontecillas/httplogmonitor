package pipeline

import (
	"context"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/alerts"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/config"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/ingest"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/output"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/stats"
)

// RunPipeline executes the log file processing based on the provided configuration.
func RunPipeline(ctx context.Context, cfg config.Config) {
	toParser := make(chan string)
	toProcFanOut := make(chan httplog.Entry)
	toStats := make(chan httplog.Entry)
	toAlerts := make(chan httplog.Entry)
	toDisplayStats := make(chan stats.Stats)
	toDisplayAlerts := make(chan alerts.Alert)

	cons := output.NewConsole()
	go DisplayOutput(ctx, toDisplayStats, toDisplayAlerts, cons, cons, cfg.DisplayRefreshTime)
	collector := stats.NewCollector(cfg.StatsTimeWindow)
	go StatsUpdater(ctx, toStats, toDisplayStats, collector)
	alerter := alerts.NewAlerter(cfg.AlertConf.TimeWindow, cfg.AlertConf.MaxRequestsPerSec)
	go AlertsUpdater(ctx, toAlerts, toDisplayAlerts, alerter)
	go HTTPLogEntryFanOut(ctx, toProcFanOut, toStats, toAlerts)
	go Parse(ctx, toParser, toProcFanOut)
	go ingest.FileIngest(ctx, cfg.FileName, toParser)
}

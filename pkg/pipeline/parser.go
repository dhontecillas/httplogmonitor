package pipeline

import (
	"context"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/ingest"
	"log"
)

// Parse receives strings and ouputs httplog.Entry's
func Parse(ctx context.Context, in chan string, out chan httplog.Entry) {
	for {
		select {
		case str := <-in:
			e, err := ingest.ParseW3CLine(str)
			if err != nil {
				log.Printf("parser error: %v", err)
			} else {
				out <- e
			}
		case <-ctx.Done():
			return
		}
	}
}

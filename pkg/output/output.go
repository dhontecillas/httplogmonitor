package output

import (
	"gitlab.com/dhontecillas/httplogmonitor/pkg/alerts"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/stats"
)

// StatsOutputer is the interface to display Stats
type StatsOutputer interface {
	SetStats(s stats.Stats)
	Display()
}

// AlertsOutputer is the interface to display Alerts
type AlertsOutputer interface {
	AlertNotification(a alerts.Alert)
	Display()
}

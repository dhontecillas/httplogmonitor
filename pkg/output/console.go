package output

import (
	"fmt"
	"log"
	"strings"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/alerts"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/stats"
)

// Console holds information to be displayed on screen in the next update.
type Console struct {
	Stats      stats.Stats
	Alerts     []alerts.Alert
	pageCount  int
	hasNewInfo bool
}

// NewConsole creates a new Console to display information using fmt.Printf
func NewConsole() *Console {
	return &Console{}
}

// SetStats updates the stats to be displayed
func (c *Console) SetStats(s stats.Stats) {
	c.hasNewInfo = true
	c.Stats = s
}

// AlertNotification is called when a new alert is triggered or shutdown
func (c *Console) AlertNotification(a alerts.Alert) {
	c.hasNewInfo = true
	if a.Stopped.IsZero() {
		// this is a new alert
		c.Alerts = append(c.Alerts, a)
		return
	}
	// if we had several possible alerts, we would need to find the one that
	// matches this one in our list, but since only one alert is possible, we
	// know that is the last one in the list
	lastAlert := len(c.Alerts) - 1
	if lastAlert < 0 || !c.Alerts[lastAlert].Stopped.IsZero() {
		log.Printf("received stopped alert, but we do not have a triggered alert")
		c.Alerts = append(c.Alerts, a)
		return
	}
	c.Alerts[lastAlert] = a
}

// Display prints on the screen the current information about stats and alerts.
func (c *Console) Display() {
	if !c.hasNewInfo {
		return
	}
	c.hasNewInfo = false
	d := time.Now()
	fmt.Printf("==== PAGE [%d] ==== [%s]\n%s\n---- End of page [%d] ---- [%s]\n\n", c.pageCount, d.String(), c.String(), c.pageCount, d.String())
	c.pageCount++
}

// String return a representation of the current state
func (c *Console) String() string {
	var sb strings.Builder
	sb.WriteString(c.Stats.String())
	sb.WriteString("ALERTS:\n")
	for idx := range c.Alerts {
		sb.WriteString(c.Alerts[idx].String())
		sb.WriteString("\n")
	}
	return sb.String()
}

package ingest

import (
	"bufio"
	"context"
	"log"
	"os"

	"github.com/fsnotify/fsnotify"
)

// ConsumeLines read lines from a file, starting `from` offset, and ouputs them
// to the `out` channel. Returns the offset to continue later from the end of
// the file.
func ConsumeLines(ctx context.Context, fname string, from int64, out chan string) (int64, error) {
	file, err := os.Open(fname)
	if err != nil {
		return 0, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.Println(err)
		}
	}()
	if _, err = file.Seek(from, 0); err != nil {
		return from, err
	}
	scanner := bufio.NewScanner(file)
	// is is assumed that the log file will never end in an unfinished line,
	// if that is the case, the lines will be truncated.
	for scanner.Scan() {
		out <- scanner.Text()
	}
	return file.Seek(0, 1)
}

// newDataWatcher sends a true through a `moreDataChan` channel to notify
// that there are new lines to be processed.
func newDataWatcher(moreDataChan chan bool, watcher *fsnotify.Watcher) {
	defer close(moreDataChan)
	// we want to make an initial first read of the file:
	moreDataChan <- true
	// and then we keep listening for the write event:
	for {
		select {
		case event, ok := <-watcher.Events:
			if !ok {
				log.Println("watcher Events channel closed")
				return
			}
			if event.Op&fsnotify.Write == fsnotify.Write {
				moreDataChan <- true
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				log.Println("watcher Errors channel closed")
				return
			}
			log.Println("watcher error:", err)
		}
	}
}

// WatchFile start watching a file for new writes, in order to parse new entries.
func WatchFile(fname string) (chan bool, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	err = watcher.Add(fname)
	if err != nil {
		return nil, err
	}

	moreDataChan := make(chan bool)
	go newDataWatcher(moreDataChan, watcher)
	return moreDataChan, nil
}

// FileIngest consumes lines from a file, and outpues them to a chan.
func FileIngest(ctx context.Context, fname string, out chan string) {
	defer close(out)
	var from int64 // the offset to read from the file

	// Watch file
	moreDataChan, err := WatchFile(fname)
	if err != nil {
		log.Printf("from: %d error: %v", from, err)
		return
	}
	for {
		select {
		case _, ok := <-moreDataChan:
			if !ok {
				return
			}
			from, err = ConsumeLines(ctx, fname, from, out)
			if err != nil {
				log.Printf("from: %d error: %v", from, err)
				return
			}
		case <-ctx.Done():
			return
		}
	}
}

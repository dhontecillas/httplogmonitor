package ingest

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
)

// ParseW3CDate gets the teim from W3C log format
func ParseW3CDate(date string) (time.Time, error) {
	layout := "02/Jan/2006:15:04:05 -0700"
	return time.ParseInLocation(layout, date, time.UTC)
}

// ParseW3CRequest converts W3C request format to httplog.Request
func ParseW3CRequest(req string) (string, []string, error) {
	split := strings.Split(req, " ")
	if len(split) < 3 {
		return "", []string{}, fmt.Errorf("bad format for request %s", req)
	}
	path := strings.Split(split[1], "/")
	if len(path) > 1 {
		path = path[1:]
	}
	return split[0], path, nil
}

// ParseW3CLine parses and returns the information from a log line
func ParseW3CLine(line string) (httplog.Entry, error) {
	split := strings.Split(line, "\"")
	if len(split) != 3 {
		return httplog.Entry{}, fmt.Errorf("bad input line (len split: %d)  line: %s", len(split), line)
	}

	dateFrom := strings.Index(split[0], "[")
	dateTo := strings.Index(split[0], "]")
	date, err := ParseW3CDate(line[dateFrom+1 : dateTo])
	if err != nil {
		return httplog.Entry{}, fmt.Errorf("bad date format %s: %s", line[dateFrom+1:dateTo], line)
	}

	pre := strings.Split(split[0][0:dateFrom-1], " ")
	if len(pre) != 3 {
		return httplog.Entry{}, fmt.Errorf("bad input line (len pre: %d): %s  line: %s", len(pre), pre, line)
	}

	method, path, err := ParseW3CRequest(split[1])
	if err != nil {
		return httplog.Entry{}, err
	}

	post := strings.Split(split[2], " ")
	if len(post) != 3 {
		return httplog.Entry{}, fmt.Errorf("bat input line (len post: %d): %s  line:  %s", len(post), post, line)
	}
	status, err := strconv.Atoi(post[1])
	if err != nil {
		return httplog.Entry{}, err
	}

	bytes, err := strconv.Atoi(post[2])
	if err != nil {
		return httplog.Entry{}, err
	}

	return httplog.Entry{
		RemoteHost: pre[0],
		RemoteUser: pre[1],
		AuthUser:   pre[2],
		Date:       date,
		Status:     status,
		Bytes:      bytes,
		Method:     method,
		Path:       path,
	}, nil
}

package ingest_test

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"testing"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/ingest"
)

func TestFileIngest(t *testing.T) {
	tmpDir, err := ioutil.TempDir("", "httplogmonitor_test_")
	if err != nil {
		t.Errorf("Can not find Temp dir")
	}
	defer func() {
		err := os.RemoveAll(tmpDir)
		if err != nil {
			log.Println(err)
		}
	}()
	file, err := ioutil.TempFile(tmpDir, "file_ingest")
	if err != nil {
		t.Errorf("Can not create Temp file: %#v", err)
	}
	defer func() {
		err := os.Remove(file.Name())
		if err != nil {
			log.Println(err)
		}
	}()

	fmt.Println(file.Name())
	_, _ = file.WriteString("L1\n")
	_ = file.Sync()

	ctx, cancel := context.WithCancel(context.Background())
	out := make(chan string)

	go ingest.FileIngest(ctx, file.Name(), out)

	d := time.Duration(100) * time.Millisecond
	time.Sleep(d)
	// check thae we already have the first line ready:
	var line string
	select {
	case line = <-out:
		if line != "L1" {
			t.Errorf("first line: %s , expected: L1", line)
		}
	default:
		t.Errorf("Should have the first line ready to be read: L1")
	}
	// check that no more lines are waiting:
	select {
	case line = <-out:
		t.Errorf("No line expected, found line: %s", line)
	default:
		time.Sleep(d)
	}

	// check that appending more lines make them appear in the chan
	_, _ = file.WriteString("L2\n")
	_ = file.Sync()
	time.Sleep(d)
	select {
	case line = <-out:
		if line != "L2" {
			t.Errorf("second line: %s , expected: L2", line)
		}
	default:
		t.Errorf("Should have the first line ready to be read: L2")
	}

	cancel()
}

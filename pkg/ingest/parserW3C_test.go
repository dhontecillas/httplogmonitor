package ingest_test

import (
	"testing"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/ingest"
)

func TestParseDate(t *testing.T) {
	date := "09/May/2018:16:00:39 +0000"
	d, err := ingest.ParseW3CDate(date)
	if err != nil {
		t.Errorf("Err %#v", err)
	}
	expected := time.Date(2018, 5, 9, 16, 0, 39, 0, time.UTC)
	if d != expected {
		t.Errorf("Wrong parsing: %#v -> %s != %s", d, d.String(), expected.String())
	}
}

func TestLineParser(t *testing.T) {
	testLines := []struct {
		input  string
		output httplog.Entry
	}{
		{"127.0.0.1 - james [09/May/2018:16:00:39 +0000] \"GET /report HTTP/1.0\" 200 123",
			httplog.Entry{
				RemoteHost: "127.0.0.1",
				RemoteUser: "-",
				AuthUser:   "james",
				Date:       time.Date(2018, 5, 9, 16, 0, 39, 0, time.UTC),
				Status:     200,
				Bytes:      123,
				Method:     "GET",
				Path:       []string{"report"},
			},
		},
		{"127.0.0.1 - jill [09/May/2018:16:00:41 +0000] \"GET /api/user HTTP/1.0\" 200 234",
			httplog.Entry{
				RemoteHost: "127.0.0.1",
				RemoteUser: "-",
				AuthUser:   "jill",
				Date:       time.Date(2018, 5, 9, 16, 0, 41, 0, time.UTC),
				Status:     200,
				Bytes:      234,
				Method:     "GET",
				Path:       []string{"api", "user"},
			},
		},
		{"127.0.0.1 - frank [09/May/2018:16:00:42 +0000] \"POST /api/user HTTP/1.0\" 200 34",
			httplog.Entry{
				RemoteHost: "127.0.0.1",
				RemoteUser: "-",
				AuthUser:   "frank",
				Date:       time.Date(2018, 5, 9, 16, 0, 42, 0, time.UTC),
				Status:     200,
				Bytes:      34,
				Method:     "POST",
				Path:       []string{"api", "user"},
			},
		},
		{"127.0.0.1 - mary [09/May/2018:16:00:42 +0000] \"POST /api/user HTTP/1.0\" 503 12",
			httplog.Entry{
				RemoteHost: "127.0.0.1",
				RemoteUser: "-",
				AuthUser:   "mary",
				Date:       time.Date(2018, 5, 9, 16, 0, 42, 0, time.UTC),
				Status:     503,
				Bytes:      12,
				Method:     "POST",
				Path:       []string{"api", "user"},
			},
		},
	}

	for _, tc := range testLines {
		res, err := ingest.ParseW3CLine(tc.input)
		if err != nil {
			t.Errorf("Failed parsing line %#v", err)
			continue
		}
		if res.Method != tc.output.Method {
			t.Errorf("Expected method: %s Got: %s", tc.output.Method, res.Method)
			continue
		}
		for idx, str := range tc.output.Path {
			if res.Path[idx] != str {
				t.Errorf("Expected path %d: %s Got: %s %#v", idx, str, res.Path[idx], res.Path)
				break
			}
		}
		if res.Date != tc.output.Date {
			t.Errorf("Expected: %s Got: %#v", tc.output.Date.String(), res.Date.String())
			continue
		}
	}
}

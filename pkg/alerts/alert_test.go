package alerts_test

import (
	"testing"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/alerts"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
)

func TestRateAlerterSpike(t *testing.T) {
	entry := httplog.Entry{
		RemoteHost: "127.0.0.1",
		RemoteUser: "-",
		AuthUser:   "james",
		Date:       time.Date(2018, 5, 9, 16, 0, 39, 0, time.UTC),
		Status:     200,
		Bytes:      123,
		Method:     "GET",
		Path:       []string{"report"},
	}

	alerter := alerts.NewAlerter(2, 5)
	t1 := entry.Date.Add(time.Duration(1) * time.Duration(time.Second))
	a := alerter.Update(t1)

	if len(a) != 0 {
		t.Errorf("No events reported, we should have empty list of alarms")
		return
	}

	// we need more than 10 requests in a window of 2 seconds, to have an alarm
	for i := 0; i < 10; i++ {
		a, err := alerter.Add(entry)
		if err != nil {
			t.Errorf("Should not give any error")
			return
		}
		if len(a) != 0 {
			t.Errorf("Should not trigger any alarm yet: added events %d", i)
			return
		}
	}

	// add older event returns an error
	oldEntry := entry
	oldEntry.Date = entry.Date.Add(-time.Duration(1) * time.Duration(time.Second))
	if _, e := alerter.Add(oldEntry); e == nil {
		t.Errorf("Stale event should return an error")
		return
	}

	a, err := alerter.Add(entry)
	if err != nil {
		t.Errorf("Should not return any error")
		return
	}
	a = alerter.Update(entry.Date.Add(time.Second))
	if len(a) != 1 {
		t.Errorf("Expected an alert when adding another event")
		return
	}
	if a[0].Triggered != entry.Date.Add(time.Second) {
		t.Errorf("The alarm should have triggered the second after the event %v , %v", a[0].Triggered, entry.Date)
		return
	}

	// if we advance one second, we won't have any alert update, because we are still
	// in the two second window
	a = alerter.Update(t1)
	if len(a) != 0 {
		t.Errorf("Should not receive alert updates, because the triggered alert still holds")
		return
	}

	// at t3, events from t0 do not longuer apply, so we get an update of the shut down alert
	t3 := t1.Add(time.Duration(2) * time.Duration(time.Second))
	a = alerter.Update(t3)
	if len(a) != 1 {
		t.Errorf("Should have an alert update, for the shutdown alarm")
		return
	}
	if a[0].Stopped.IsZero() {
		t.Errorf("We should havea valid Stopped time")
		return
	}
}

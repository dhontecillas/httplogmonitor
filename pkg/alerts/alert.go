package alerts

import (
	"fmt"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
)

// Alert contains information about an alert triggered
type Alert struct {
	Triggered time.Time
	Stopped   time.Time
	Message   string
}

// String provides a representation for an alert
func (a *Alert) String() string {
	format := "02/Jan/2006:15:04:05 -0700"
	if a.Stopped.IsZero() {
		return fmt.Sprintf("%s triggered at %s  ✗", a.Message, a.Triggered.Format(format))
	}
	return fmt.Sprintf("%s triggered at %s  ✔ (%s)", a.Message, a.Triggered.Format(format), a.Stopped.Format(format))
}

// Alerter is the interface required to consume log entries and raise alerts
type Alerter interface {
	Add(e httplog.Entry) ([]Alert, error)
	Update(t time.Time) []Alert
}

// RateAlerter creates alert for high rate with high hit rate
type RateAlerter struct {
	timeWindow       time.Duration // time window for the rate limit
	maxReqsInWindow  int           // instead of having the reqs per second, we precompute the number or req per time window
	hitTimestamps    []time.Time   // the sorted list of times for every hit inside the time window
	activeAlert      *Alert        // active alerts
	lastReceivedTime time.Time     // to make sure we do not receive non sorted events
}

// NewAlerter creates a new Alerter to trigger alerts
func NewAlerter(timeWindowSecs int, maxReqsPerSec int) *RateAlerter {
	return &RateAlerter{
		timeWindow:      -(time.Duration(timeWindowSecs) * time.Second),
		maxReqsInWindow: maxReqsPerSec * timeWindowSecs,
		hitTimestamps:   []time.Time{},
	}
}

// Add a new event to the alerting system. Returns an slice with
// updated alerts.
func (ra *RateAlerter) Add(e httplog.Entry) ([]Alert, error) {
	if e.Date.Before(ra.lastReceivedTime) {
		return []Alert{}, fmt.Errorf("received stale event %#v last event date: %s", e, ra.lastReceivedTime.String())
	}
	ra.lastReceivedTime = e.Date
	stale := ra.lastReceivedTime.Add(ra.timeWindow)
	ra.hitTimestamps = removeStaleTimes(append(ra.hitTimestamps, e.Date), stale)
	return ra.Update(ra.lastReceivedTime), nil
}

// Update checks for changes in Alerts
func (ra *RateAlerter) Update(t time.Time) []Alert {
	t = t.Truncate(time.Second)
	alerts := []Alert{}
	stale := t.Add(ra.timeWindow)
	ra.hitTimestamps = removeStaleTimes(ra.hitTimestamps, stale)
	cnt := 0
	for cnt = 0; cnt < len(ra.hitTimestamps) && ra.hitTimestamps[cnt].Before(t); cnt++ {
	}
	if cnt > ra.maxReqsInWindow {
		if ra.activeAlert == nil {
			ra.activeAlert = &Alert{
				Triggered: t,
				Message:   fmt.Sprintf("High traffic generated an alert - hits = %d,", cnt),
			}
			alerts = append(alerts, *ra.activeAlert)
		}
	} else {
		if ra.activeAlert != nil {
			ra.activeAlert.Stopped = t
			alerts = append(alerts, *ra.activeAlert)
			ra.activeAlert = nil
		}
	}
	return alerts
}

func removeStaleTimes(times []time.Time, stale time.Time) []time.Time {
	idx := 0
	for idx = 0; idx < len(times) && times[idx].Before(stale); idx++ {
	}
	return times[idx:]
}

package config_test

import (
	"testing"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/config"
)

func TestDefaultValues(t *testing.T) {
	c := config.NewConfig()
	if c.DisplayRefreshTime != 10 {
		t.Errorf("Test error")
	}
}

func BenchmarkDefaultValues(b *testing.B) {
	config.NewConfig()
}

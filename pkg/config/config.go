package config

// RateAlert contains the configuration for a high rate alret
type RateAlert struct {
	TimeWindow        int // Number of seconds for time window to observe
	MaxRequestsPerSec int // Number or request in the time window that triggers the alert
}

// Config contains the configuration for running the tool
type Config struct {
	FileName           string    // file to monitor
	DisplayRefreshTime int       // time between display refreshes
	StatsTimeWindow    int       // number of seconds for the recent stats
	AlertConf          RateAlert // configuration for the alert
}

// NewConfig creates new Config with default values
func NewConfig() Config {
	return Config{
		FileName:           "/tmp/access.log",
		DisplayRefreshTime: 10,
		StatsTimeWindow:    10,
		AlertConf: RateAlert{
			TimeWindow:        120,
			MaxRequestsPerSec: 10,
		},
	}
}

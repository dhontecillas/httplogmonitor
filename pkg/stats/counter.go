package stats

import "sort"

// Pair is a string Key with an int Value
type Pair struct {
	Key   string
	Value int
}

// PairList is list of Pair
type PairList []Pair

// Len return the length of the PairList: interface required for the sort
func (p PairList) Len() int {
	return len(p)
}

// Less compares two elements of the PairList: interface required for the sort
func (p PairList) Less(i, j int) bool {
	return p[i].Value < p[j].Value
}

// Swap exchenges the values of two Pairs: interface required for the sort
func (p PairList) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

// Counter is a structure to maintain the number of times a string appears,
// and allows only positive counting
type Counter struct {
	Count map[string]int
}

// NewCounter creates new Counter
func NewCounter() *Counter {
	return &Counter{
		Count: map[string]int{},
	}
}

// Inc increments the counter for a string
func (c *Counter) Inc(key string) {
	c.Count[key]++
}

// Dec decrements the counter for a string
func (c *Counter) Dec(key string) {
	c.Count[key]--
	if c.Count[key] == 0 {
		delete(c.Count, key)
	}
}

// Sorted returns the list of string by number of counts
func (c *Counter) Sorted() PairList {
	pl := make(PairList, len(c.Count))
	i := 0
	for k, v := range c.Count {
		pl[i] = Pair{k, v}
		i++
	}
	sort.Sort(sort.Reverse(pl))
	return pl
}

package stats

import (
	"fmt"
	"strings"
	"time"
)

// GlobalSummary holds information for all entries found
type GlobalSummary struct {
	Requests          int
	Bytes             int
	StatusOK          int
	StatusServerError int
}

// Stats contains meaningful statistics to be displayed
type Stats struct {
	At         time.Time     // When these stats where collected
	TimeWindow time.Duration // The duration fo the time window for these stats
	NumEntries int           // Num of total entries
	TotalBytes int           // Number of total transmited bytes
	TopTraffic PairList      // Top sections with more hits
	Global     GlobalSummary // Information about all received traffic
}

// String returns a readable string displaying the stats
func (s *Stats) String() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("GLOBAL - Num Reqs: %d Num Bytes: %d   STATUS: OK: %d   Server Err: %d  Others: %d\n",
		s.Global.Requests, s.Global.Bytes, s.Global.StatusOK, s.Global.StatusServerError,
		s.Global.Requests-s.Global.StatusOK-s.Global.StatusServerError))
	sb.WriteString(fmt.Sprintf("At: %s (%v) Hits: %d Bytes: %d\n", s.At.Format("02/Jan/2006:15:04:05 -0700"), s.TimeWindow, s.NumEntries, s.TotalBytes))

	if len(s.TopTraffic) > 0 {
		sb.WriteString(fmt.Sprintf("TOP %d:\n", len(s.TopTraffic)))
		for idx := range s.TopTraffic {
			sb.WriteString(fmt.Sprintf("- %s : %d\n", s.TopTraffic[idx].Key, s.TopTraffic[idx].Value))
		}
	}
	return sb.String()
}

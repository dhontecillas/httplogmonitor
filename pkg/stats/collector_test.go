package stats_test

import (
	"testing"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
	"gitlab.com/dhontecillas/httplogmonitor/pkg/stats"
)

func TestStatSingleLogEntry(t *testing.T) {
	entry := httplog.Entry{
		RemoteHost: "127.0.0.1",
		RemoteUser: "-",
		AuthUser:   "james",
		Date:       time.Date(2018, 5, 9, 16, 0, 39, 0, time.UTC),
		Status:     200,
		Bytes:      123,
		Method:     "GET",
		Path:       []string{"report"},
	}

	// window of two seconds
	c := stats.NewCollector(2)
	s := c.Update(entry.Date)
	if s.NumEntries != 0 {
		t.Errorf("should be empty")
	}

	c.Add(entry)
	s = c.Update(entry.Date)
	if s.NumEntries != 0 {
		t.Errorf("num entries expected: 0 , got: %d", s.NumEntries)
		return
	}

	s = c.Update(entry.Date.Add(time.Duration(1) * time.Second))
	if s.NumEntries != 1 {
		t.Errorf("after 1s we should see the previous stats")
		return
	}
	if len(s.TopTraffic) != 1 {
		t.Errorf("page should appear in top traffic")
		return
	}
	if s.TopTraffic[0].Key != "report" {
		t.Errorf("bad page name, expected report, found: %s", s.TopTraffic[0].Key)
		return
	}

	s = c.Update(entry.Date.Add(time.Duration(2) * time.Second))
	if s.NumEntries != 1 {
		t.Errorf("after 2s it displays for buckets in second 0 and 1")
		return
	}
	if len(s.TopTraffic) != 1 {
		t.Errorf("page should still be visible %#v", s.TopTraffic)
		return
	}

	s = c.Update(entry.Date.Add(time.Duration(3) * time.Second))
	if s.NumEntries != 0 {
		t.Errorf("after 3s the event should not be visible any longer")
		return
	}
	if len(s.TopTraffic) != 0 {
		t.Errorf("there should not be top traffic %#v", s.TopTraffic)
		return
	}
}

func TestStatsCollector(t *testing.T) {
	testEntries := []httplog.Entry{
		httplog.Entry{
			RemoteHost: "127.0.0.1",
			RemoteUser: "-",
			AuthUser:   "james",
			Date:       time.Date(2018, 5, 9, 16, 0, 39, 0, time.UTC),
			Status:     200,
			Bytes:      123,
			Method:     "GET",
			Path:       []string{"report"},
		},
		httplog.Entry{
			RemoteHost: "127.0.0.1",
			RemoteUser: "-",
			AuthUser:   "jill",
			Date:       time.Date(2018, 5, 9, 16, 0, 41, 0, time.UTC),
			Status:     200,
			Bytes:      234,
			Method:     "GET",
			Path:       []string{"api", "user"},
		},
		httplog.Entry{
			RemoteHost: "127.0.0.1",
			RemoteUser: "-",
			AuthUser:   "frank",
			Date:       time.Date(2018, 5, 9, 16, 0, 42, 0, time.UTC),
			Status:     200,
			Bytes:      34,
			Method:     "POST",
			Path:       []string{"api", "user"},
		},
		httplog.Entry{
			RemoteHost: "127.0.0.1",
			RemoteUser: "-",
			AuthUser:   "mary",
			Date:       time.Date(2018, 5, 9, 16, 0, 42, 0, time.UTC),
			Status:     503,
			Bytes:      12,
			Method:     "POST",
			Path:       []string{"api", "user"},
		},
	}
	c := stats.NewCollector(10)

	for _, t := range testEntries {
		c.Add(t)
	}
	s := c.Update(testEntries[3].Date.Add(time.Second))

	if s.NumEntries != 4 {
		t.Errorf("Expected 4 entries in the las 10 seconds, found: %d", s.NumEntries)
	}
	if len(s.TopTraffic) != 2 {
		t.Errorf("Expected 2 entries in top traffic, found %#v", s.TopTraffic)
	}
	if s.TopTraffic[0].Key != "api" || s.TopTraffic[0].Value != 3 {
		t.Errorf("Bad top request")
	}
}

package stats_test

import (
	"testing"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/stats"
)

func TestStatsCounter(t *testing.T) {
	c := stats.NewCounter()

	test := []string{"a", "a", "b", "c", "a", "b", "a", "c", "d"}
	for _, v := range test {
		c.Inc(v)
	}
	res := c.Sorted()
	if res[0].Key != "a" {
		t.Error("'a' should be the first element")
	}
}

package stats

import (
	"fmt"
	"log"
	"strings"
	"time"

	"gitlab.com/dhontecillas/httplogmonitor/pkg/httplog"
)

/*
- Displays stats every 10s about the traffic during those 10s: the sections of
  the web site with the most hits, as well as interesting summary statistics
  on the traffic as a whole. A section is defined as being what's before the
  second '/' in the resource section of the log line.
  For example, the section for "/pages/create" is "/pages"
*/

// Collector ingests data and computes Stats
type Collector interface {
	Update(curTime time.Time) Stats
	Add(e httplog.Entry)
}

// TimeWindowCollector collects stats for a give time window
type TimeWindowCollector struct {
	timeWindow    time.Duration   // time windows for whe the stats are computed
	numTopTraffic int             // number of top pages by number of hits to display
	entries       []httplog.Entry // sorted list of input entries fot the time window
	globalSummary GlobalSummary
}

// NewCollector creates a new TimeWindowCollector
func NewCollector(windowSeconds int) *TimeWindowCollector {
	return &TimeWindowCollector{
		timeWindow:    -time.Duration(windowSeconds) * time.Second,
		numTopTraffic: 10,
		globalSummary: GlobalSummary{},
	}
}

// Update updates the stats at the curTime. The clock for
// the updated is provided externally
func (c *TimeWindowCollector) Update(curTime time.Time) Stats {
	// we must remove all those entries that are outside the timeWindow
	tm := curTime.Truncate(time.Second)
	stale := tm.Add(c.timeWindow)
	var idx int
	for idx = 0; idx < len(c.entries) && c.entries[idx].Date.Before(stale); idx++ {
	}
	c.entries = c.entries[idx:]
	return c.compute(tm)
}

// compute return computes the stats for the current data
func (c *TimeWindowCollector) compute(t time.Time) Stats {
	hits := NewCounter()
	var cnt = 0
	totalBytes := 0
	for cnt = 0; cnt < len(c.entries) && c.entries[cnt].Date.Before(t); cnt++ {
		totalBytes += c.entries[cnt].Bytes
		hits.Inc(c.entries[cnt].Path[0])
	}
	topTraffic := hits.Sorted()
	if len(topTraffic) > c.numTopTraffic {
		topTraffic = topTraffic[:c.numTopTraffic]
	}
	return Stats{
		At:         t,
		TimeWindow: c.timeWindow,
		NumEntries: cnt,
		TotalBytes: totalBytes,
		TopTraffic: topTraffic,
		Global:     c.globalSummary,
	}
}

func (c *TimeWindowCollector) updateGlobalSummary(e httplog.Entry) {
	c.globalSummary.Requests++
	c.globalSummary.Bytes += e.Bytes
	if e.Status >= 200 && e.Status < 300 {
		c.globalSummary.StatusOK++
	} else if e.Status >= 500 {
		c.globalSummary.StatusServerError++
	}
}

// Add appends a new Entry to the stats collector
func (c *TimeWindowCollector) Add(e httplog.Entry) {
	// Checking precondition: if the event is sooner than any events we have,
	// we should insert the event sorted in the array
	if len(c.entries) > 0 && e.Date.Before(c.entries[len(c.entries)-1].Date) {
		log.Printf("Assumption log entries are time sorted not met")
		return
	}
	c.entries = append(c.entries, e)
	c.updateGlobalSummary(e)
}

func (c *TimeWindowCollector) String() string {
	var sb strings.Builder
	for _, e := range c.entries {
		sb.WriteString(fmt.Sprintf("[%s] %s\n", e.Date.Format("02/Jan/2006:15:04:05 -0700"), e.AuthUser))
	}
	return sb.String()
}

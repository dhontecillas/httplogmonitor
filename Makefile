PACKAGES         ?= $(shell go list ./... | grep -v vendor | grep -v gopath)
GOTOOLS          ?= github.com/GeertJohan/fgt\
					golang.org/x/tools/cmd/goimports\
					github.com/kisielk/errcheck \
					golang.org/x/lint/golint \
					github.com/wadey/gocovmerge \
					github.com/fsnotify/fsnotify  \
					honnef.co/go/tools/cmd/staticcheck


lint: tools
	$(GOROOT)/bin/fgt go fmt $(PACKAGES)
	$(GOROOT)/bin/fgt go vet $(PACKAGES)
	$(GOROOT)/bin/fgt $(GOROOT)/bin/errcheck -ignore Close $(PACKAGES)
	echo $(PACKAGES) | xargs -L1 $(GOROOT)/bin/fgt $(GOROOT)/bin/golint
	$(GOROOT)/bin/staticcheck $(PACKAGES)
.SILENT: lint

tools:
	go get $(GOTOOLS)
.SILENT: tools

bench:
	go test -bench=. $(PACKAGES)

test:
	go test $(PACKAGES)

check: test lint

build:
	go build -v ./cmd/httplogmonitor
.PHONY: build

buildgen:
	go build -v ./cmd/httploggen
.PHONY: buildgen

docker:
	docker build . -t httplogmonitor

dockerrun:
	mkdir -p ./tmp
	touch ./tmp/access.log
	docker run -v $(shell pwd)/tmp:/tmp httplogmonitor

coverage:
	mkdir -p coverage
	mkdir -p docs/coverage
	$(foreach pkg,$(PACKAGES),\
	go test $(pkg) -coverprofile="coverage/$(shell echo $(pkg) | tr "\/" _)" -coverpkg=$(go list ./... | grep -v /assets/ | paste -sd "," -) -covermode=set;)
	gocovmerge coverage/* > coverage/aggregate.coverprofile
	go tool cover -html=coverage/aggregate.coverprofile -o docs/coverage/coverage.html
	@echo ----------------------------------------
	@echo COVERAGE IS AT: $$(go tool cover -func=coverage/aggregate.coverprofile | tail -n 1 | rev | cut -d" " -f1 | rev)
	@echo ----------------------------------------
	rm -rf coverage
	xdg-open docs/coverage/coverage.html
